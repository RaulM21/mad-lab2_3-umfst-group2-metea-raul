package com.example.ex4_bookapplication;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class Chapter4Fragment extends Fragment {

    private TextView chapter4TitleTextView, chapter4ContentTextView;
    private ImageView chapter4ImageView;

    public Chapter4Fragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_capitolul4, container, false);

        chapter4TitleTextView = view.findViewById(R.id.chapter4_title_textview);
        chapter4ContentTextView = view.findViewById(R.id.chapter4_content_textview);
        chapter4ImageView = view.findViewById(R.id.chapter4_imageview);

        chapter4TitleTextView.setText("Chapter 4");
        chapter4ContentTextView.setText("The police thanked us for our good work.\n" + "\n" + "By that time, our parents had come home. We told them the whole story about the robbery. My parents were thankful that we were not hurt but they lectured me for not stopping my brother from opening the door to strangers.");
        chapter4ImageView.setImageResource(R.drawable.the_end_img);

        return view;
    }

}
