package com.example.ex4_bookapplication;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class Chapter2Fragment extends Fragment {

    private TextView chapter2TitleTextView, chapter2ContentTextView;
    private ImageView chapter2ImageView;

    public Chapter2Fragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_capitolul2, container, false);

        chapter2TitleTextView = view.findViewById(R.id.chapter2_title_textview);
        chapter2ContentTextView = view.findViewById(R.id.chapter2_content_textview);
        chapter2ImageView = view.findViewById(R.id.chapter2_imageview);

        chapter2TitleTextView.setText("Chapter 2");
        chapter2ContentTextView.setText("Without thinking, my brother said, \"No.\" He asked if we would like to buy some comic books, which he was selling.");
        chapter2ImageView.setImageResource(R.drawable.ch2_img);

        return view;
    }

}
