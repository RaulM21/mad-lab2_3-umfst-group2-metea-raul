package com.example.ex4_bookapplication;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class Chapter1Fragment extends Fragment {

    private TextView chapter1TitleTextView, chapter1ContentTextView;
    private ImageView chapter1ImageView;

    public Chapter1Fragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_capitolul1_, container, false);

        chapter1TitleTextView = view.findViewById(R.id.chapter1_title_textview);
        chapter1ContentTextView = view.findViewById(R.id.chapter1_content_textview);
        chapter1ImageView = view.findViewById(R.id.chapter1_imageview);

        chapter1TitleTextView.setText("Chapter 1");
        chapter1ContentTextView.setText("My brother and I were alone in our apartment. My parents had gone for a function and had left me in charge. My younger brother rushed to the door thinking that our parents had come home; he unlocked the door and opened it.");
        chapter1ImageView.setImageResource(R.drawable.ch1_imag);

        return view;
    }

}
