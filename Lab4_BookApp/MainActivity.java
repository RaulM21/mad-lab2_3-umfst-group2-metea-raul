package com.example.ex4_bookapplication;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    private Button chapter1Button, chapter2Button, chapter3Button, chapter4Button;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chapter1Button = findViewById(R.id.chapter1_button);
        chapter2Button = findViewById(R.id.chapter2_button);
        chapter3Button = findViewById(R.id.chapter3_button);
        chapter4Button = findViewById(R.id.chapter4_button);

        chapter1Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new Chapter1Fragment());
            }
        });

        chapter2Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new Chapter2Fragment());
            }
        });

        chapter3Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new Chapter3Fragment());
            }
        });

        chapter4Button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                loadFragment(new Chapter4Fragment());
            }
        });

    }

    private void loadFragment(Fragment fragment) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, fragment)
                .commit();
    }
}
