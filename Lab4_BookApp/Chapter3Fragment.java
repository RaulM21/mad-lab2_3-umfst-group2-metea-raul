package com.example.ex4_bookapplication;

import android.os.Bundle;
import androidx.fragment.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class Chapter3Fragment extends Fragment {

    private TextView chapter3TitleTextView, chapter3ContentTextView;
    private ImageView chapter3ImageView;

    public Chapter3Fragment() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_capitolul3, container, false);

        chapter3TitleTextView = view.findViewById(R.id.chapter3_title_textview);
        chapter3ContentTextView = view.findViewById(R.id.chapter3_content_textview);
        chapter3ImageView = view.findViewById(R.id.chapter3_imageview);

        chapter3TitleTextView.setText("Chapter 3");
        chapter3ContentTextView.setText("Soon, he went upstairs to search the bedroom for valuables. I managed to teach my brother to untie the rope on his hands.");
        chapter3ImageView.setImageResource(R.drawable.ch3_img);

        return view;
    }

}
