package com.example.dab_lab1_proba;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    int nrClick=0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void OnClickToast(View view)
    {
        Log.i("My app", "button works");
        Toast.makeText(getApplicationContext(), "Button start", Toast.LENGTH_SHORT).show();
    }

    public void onClickStart(View view)
    {
        TextView myText = findViewById(R.id.textView);
        String str = String.valueOf(nrClick);
        myText.setText(str);
        nrClick++;
    }
}