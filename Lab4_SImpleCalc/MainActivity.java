package com.example.lab4simplecalcapp;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private EditText nr1;
    private EditText nr2;
    private Button add,substract,multiply,divide;
    private TextView result;

    @SuppressLint({"WrongViewCast", "MissingInflatedId"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nr1 = findViewById(R.id.textN1);
        nr2= findViewById(R.id.textN2);
        add=findViewById(R.id.buttonAdd);
        substract=findViewById(R.id.buttonSub);
        divide=findViewById(R.id.buttonDiv);
        multiply=findViewById(R.id.buttonMul);
        result = findViewById(R.id.result);


        add.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double num1=Double.parseDouble(nr1.getText().toString());
                double num2=Double.parseDouble((nr2.getText().toString()));

                double add=num1 + num2;
                result.setText(String.valueOf(add));
            }
        });

        substract.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double num1=Double.parseDouble(nr1.getText().toString());
                double num2=Double.parseDouble((nr2.getText().toString()));

                double substract=num1 - num2;
                result.setText(String.valueOf(substract));
            }
        });

        divide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double num1=Double.parseDouble(nr1.getText().toString());
                double num2=Double.parseDouble((nr2.getText().toString()));

                if(num2 ==0)
                {
                    result.setText("0 cannot be divided");
                }else {
                    double divide = num1 / num2;
                    result.setText(String.valueOf(divide));
                }
            }
        });

        multiply.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                double num1=Double.parseDouble(nr1.getText().toString());
                double num2=Double.parseDouble((nr2.getText().toString()));

                double multiply=num1 * num2;
                result.setText(String.valueOf(multiply));
            }
        });
    }
}